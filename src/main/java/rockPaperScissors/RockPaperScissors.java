package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    boolean win = false;
    List<String> validAnswers = Arrays.asList("y", "n");
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public String randomChoice() {
        Random random = new Random();
        int myRange = 3;
        int randomNum = random.nextInt(myRange);
        String computerChoice = rpsChoices.get(randomNum);
        return computerChoice;
    }
    
    /** 
     * Check if choice1 is wins over choice2
     * Returns true if choice1 beats choice2, false if not.
     */
    public boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("paper")){
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")){
            return choice2.equals("paper");
        }
        else {
            return choice2.equals("scissors");
        }
    }
    /**
     * Prompt the user with what choice of rock paper scissors they choose. 
     * Returns "rock", "paper" or "scissors"
     */
    public String userChoice() {
        while (true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (validateInput(humanChoice, rpsChoices)){
                return humanChoice;
            }
            else {
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
            }
        }
    }
    /**
     * Prompt the user if they want to continue playing. 
     * Returns "y" or "no".
     */
    public String continuePlaying() {
        while (true){
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (validateInput(continueAnswer, validAnswers)){
                return continueAnswer;
            }
            else {
                System.out.println("I do not understand " + continueAnswer + ". Could you try again?");
            }
    }

        }
    /**
     * Checks if the given input is either rock, paper or scissors.
     * Returns: true if valid input, false if not
     */
    public boolean validateInput(String input, List<String> validInput) {
        input = input.toLowerCase();
        return validInput.contains(input);
    }
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            // Human and Computer choice
            String humanChoice = userChoice();
            String computerChoice = randomChoice();
            String choiceString = "Human chose " + humanChoice + ", computer chose " + computerChoice + ".";
            // Check who won
            if (isWinner(humanChoice, computerChoice)) {
                System.out.println(choiceString + " Human wins!");
                humanScore += 1;
            }
            else if (isWinner(computerChoice, humanChoice)) {
                System.out.println(choiceString + " Computer wins!");
                computerScore += 1;
            }
            else {
                System.out.println(choiceString + " It's a tie!");

            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            // Ask if human wants to play again
            String continueAnswer = continuePlaying();
            if (continueAnswer.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
            roundCounter += 1;
        }
        }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    
}
